# def_bubbleSort.py

def bubble_sort(list):
    for i in range(len(list)):
        sorted = True
        # Iterate over the range of the length of the list minus i minus 1
        for j in range(len(list) - i - 1):
            # If the current element is greater than the next element
            if list[j] > list[j+1]:
                # Swap the current element with the next element
                list[j], list[j+1] =  list[j+1], list[j]
                sorted = False
        # If the list is already sorted, break out of the loop
        if sorted:
            break
    return list

lists = [1, 5, 7, 9, 2, 3, 6, 1, 1, 0]
print(lists)
# [1, 5, 7, 9, 2, 3, 6, 1, 1, 0]

bubble_sort(lists)
print(lists)
# [0, 1, 1, 1, 2, 3, 5, 6, 7, 9]